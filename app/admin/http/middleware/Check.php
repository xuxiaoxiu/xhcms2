<?php
/**
 * 后台验证权限中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\http\middleware;
use app\admin\db\Config as ConfigDb;
use think\facade\Config;
use think\facade\Cache;

class Check
{
	
    public function handle($request, \Closure $next)
    {	
		$pathinfo = explode('/',$request->pathinfo());
		$controller = !empty($pathinfo[0]) ? str_replace('.html','',$pathinfo[0]) : 'Index';
		$action = !empty($pathinfo[1]) ? str_replace('.html','',$pathinfo[1]) : 'index';
		$app = app('http')->getName();
		
		$admin = session('admin');
        $userid = session('admin_sign') == data_auth_sign($admin) ? $admin['userid'] : 0;
		
        if( !$userid && ( $app <> 'admin' || $controller <> 'Login' )){
			return redirect(url('admin/Login/index'));
        }
		
		//验证权限
		$url =  "/{$app}/{$controller}/{$action}";
		
		 /*不需要检测的权限*/		
		$nocheck = config('my.nocheck');
		if(session('admin.role') !== 1 && !in_array($url,$nocheck) && $action !== 'startImport' && $action !== 'getExtends'){	
			if($controller <> 'FormData'){
				if(!in_array($url,session('admin.nodes'))){
					return json(['status'=>'01','msg'=>'你没权限访问']);
				}	
			}else{
				//表单管理权限单独检测
				$extend_id = input('param.extend_id');
				if(!in_array('/admin/FormData/'.$action.'/extend_id/'.$extend_id.'.html',session('admin.nodes')) && $action <> 'getExtends'){
					return json(['status'=>'01','msg'=>'你没权限访问']);
				}
			}
		}
		
		//写入配置
		$list = ConfigDb::loadList();
		Config::set($list,'xhadmin');
		
        return $next($request);
    }
}