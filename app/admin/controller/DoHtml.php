<?php 

namespace app\admin\controller;
use app\admin\controller\Admin;
use cms\service\CatagoryService;
use app\index\service\BaseService;
use cms\db\Catagory;
use cms\db\Content;
use app\index\facade\Cat;
use think\facade\Config;

class DoHtml extends Admin {
	
	
	
	public function index(){
		$this->view->assign('tpList',CatagoryService::tplList(config('xhadmin.default_themes')));
		return $this->display('info');
	}
	
	//生成首页
	public function doIndex(){
		if ($this->request->isPost()){
			Config::set(['url_type' => 2], 'xhadmin');
			$index_tpl = input('param.index_tpl','','strval');
			!$index_tpl && $this->error('首页模板不能为空');
			$this->view->assign('media', $media=baseService::getMedia());  //网站关键词描述信息
			try{
				$index_name = config('xhadmin.index_name') ? config('xhadmin.index_name') : 'index.html';
				$this->filePutContents('./'.$index_name,$index_tpl);
			}catch(\Exception $e){
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'生成成功']);
		}	
	} 
	
	//生成列表页
	public function doList(){
		Config::set(['url_type' => 2], 'xhadmin');
		$classId = input('param.classId','','intval');
		if (!$this->request->isPost()){
			$this->view->assign('classId',$classId);
			return $this->display('listprocess');
		}else{
			if(!$classId){
				$page = input('param.page','','intval');
				$info = db("Catagory")->limit($page-1,1)->where('list_tpl','<>','')->order('sortid asc')->select()->toArray();
				$info = current($info);
				$count = Catagory::countList($where);
				if($info){				
					try{	
						$this->getListContent($info);
						$dt['percent'] = ceil($page/$count*100);
						$dt['filename'] = $info['filepath'].'/'.$info['filename'];
						return json(['error'=>'00','data'=>$dt]);
					}catch(\Exception $e){
						exit($this->error($e->getMessage()));
					}	
				}else{
					return json(['error'=>'10']);
				}	
			}else{				
				$info = Catagory::getInfo($classId);
				$count = 1;
				$page = 1;
				if($info){				
					try{	
						$this->getListContent($info);
						$dt['percent'] = ceil($page/$count*100);
						$dt['filename'] = $info['filepath'].'/'.$info['filename'];
						return json(['error'=>'00','data'=>$dt]);
					}catch(\Exception $e){
						exit($this->error($e->getMessage()));
					}	
				}			
			}	
		}
	}
	
	//获取列表页内容
	private function getListContent($info){
		config('xhadmin.url_type') == 2;
		try{
			$class_id = $info['class_id'];
			$position = Cat::getPosition($class_id);
			$topCategoryInfo = Cat::getTopBigInfo($class_id); //最上级栏目信息
			$this->view->assign('media',baseService::getMedia($info['class_name'])); 
			$this->view->assign('info',$info);
			$this->view->assign('class_name',$info['class_name']); 
			$this->view->assign('classid',$info['class_id']);	
			$this->view->assign('pname',$topCategoryInfo['class_name']);
			$this->view->assign('pid',$topCategoryInfo['class_id']);
			$this->view->assign('position', $position);
			$this->view->assign('sub_data', Catagory::countList(['pid'=>$topCategoryInfo['class_id']])); //判断是否有子分类
			$this->view->assign('p',1);
			if($info['type'] == 1){
				$this->view->assign('info',Content::getWhereInfo(['class_id'=>$class_id]));
			}
			$filepath = './'.$info['filepath'].'/'.$info['filename'];
			$this->filePutContents($filepath,$info['list_tpl']);
			//判断是否列表
			if($info['type'] == 2){
				$idx = Cat::getSubClassId($info['class_id']);
				$contentWhere['class_id'] = explode(',',$idx);
				$contentWhere['status'] = 1;
				$contentCount = Content::countList($contentWhere);
				if($contentCount > 0){
					$pagesize = $this->getListPageSize($info);
					!$pagesize && $pagesize = 10;
					$totalpage = ceil($contentCount/$pagesize);
					if($totalpage > 1){
						for ($i=2; $i <=$totalpage; $i++){
							$this->view->assign('p',$i);
							$filepath = './'.$info['filepath'].'/'.$i.'/'.$info['filename'];
							$this->filePutContents($filepath,$info['list_tpl']);
						}
					}
				}	
			}
			$position = '';
		}catch(\Exception $e){
			throw new \Exception($e->getMessage());
		}	
	}
	
	//生成详情页
	public function doView(){
		Config::set(['url_type' => 2], 'xhadmin');
		$classId = input('param.classId','','intval');
		$startId = input('param.startId','','intval');
		$endId = input('param.endId','','intval');
		$pagesize = input('param.pagesize','','intval');
		
		if (!$this->request->isPost()){
			$this->view->assign('classId',$classId);
			$this->view->assign('pagesize',$pagesize);
			$this->view->assign('startId',$startId);
			$this->view->assign('endId',$endId);
			return $this->display('viewprocess');
		}else{
			try{
				$page = input('param.page',1,'intval');
				$classId && $where['a.class_id'] = $classId;
				$startId && $where['a.content_id'] = ['>=',$startId];
				$endId && $where['a.content_id'] = ['<=',$endId];
				$where['b.type'] = 2;
				$start=($page-1) * $pagesize;
				$contentList = db('content')->alias('a')->join('catagory b','a.class_id=b.class_id')->where(formatWhere($where))->field('a.*,b.*')->limit($start,$pagesize)->order('content_id asc')->select()->toArray();
				$count = db('content')->alias('a')->join('catagory b','a.class_id=b.class_id')->where(formatWhere($where))->limit($start,$pagesize)->order('content_id asc')->count();
				
				$per = ceil($count/$pagesize);
				if($contentList){
					foreach($contentList as $key=>$val){
						if($val['type'] == 2 && !empty($val['list_tpl']) || !empty($val['detail_tpl']) && !empty($val['jumpurl'])){
							$topCategoryInfo = Cat::getTopBigInfo($val['class_id']); //最上级栏目信息
							//获取拓展模块的内容信息
							if($val['module_id']){
								$extInfo = \cms\service\ContentService::getExtDataInfo($val['module_id'],$val['content_id']);
								if($extInfo){
									unset($extInfo['data_id'],$extInfo['content_id']);
									$val = array_merge($val , $extInfo);
								}	
							}
							$this->view->assign('media',BaseService::getMedia($val['title'])); //关键词描述等信息
							$this->view->assign('classInfo',$val);  //当前栏目信息
							$this->view->assign('class_name',$val['class_name']);  //当前栏目名称
							$this->view->assign('classid',$val['class_id']);	//当前栏目ID
							$this->view->assign('pname',$topCategoryInfo['class_name']);  //最上级栏目名称
							$this->view->assign('pid',$topCategoryInfo['class_id']);	//最上级栏目ID
							$this->view->assign('position',$this->getPos($val['class_id'])); //面包屑信息
							$this->view->assign('info',$val);
							$this->view->assign('shownext', BaseService::shownext($val['content_id'],$val['class_id']));
							$this->view->assign('sub_data', Catagory::countList(['pid'=>$topCategoryInfo['class_id']])); //判断是否有子分类
							$filepath = './'.$val['filepath'].'/'.$val['content_id'].'.html';
							$this->filePutContents($filepath,$val['detail_tpl']);
						}
					}
					$dt['filename'] = $filepath;
					$dt['percent'] = ceil($page/$per*100);
					return json(['error'=>'00','data'=>$dt]);
				}else{
					return json(['error'=>'10']);
				}
			}catch(\Exception $e){
				throw new \Exception($e->getMessage());
			}
		}
	}
	
	//没有解决面包重复问题 有待改进
	private function getPos($classId){
		$info = Catagory::getInfo($classId);
		$parentInfo = Catagory::getInfo($info['pid']);
		$topBigInfo = Catagory::getInfo($parentInfo['pid']);
		$pos = '当前位置：<a href="'.url('@index').'">首页</a>&nbsp;&gt;&gt;&nbsp;';
		if($topBigInfo){
			$pos .= '<a href="'.U($topBigInfo['class_id']).'">'.$topBigInfo['class_name'].'</a>&nbsp;&gt;&gt;&nbsp;<a href="'.U($parentInfo['class_id']).'">'.$parentInfo['class_name'].'</a>&nbsp;&gt;&gt;&nbsp;<a href="'.U($info['class_id']).'">'.$info['class_name'].'</a>';
		}else{
			if($parentInfo){
				$pos .= '<a href="'.U($parentInfo['class_id']).'">'.$parentInfo['class_name'].'</a>'.'&nbsp;&gt;&gt;&nbsp;<a href="'.U($info['class_id']).'">'.$info['class_name'].'</a>';
			}else{
				$pos .= '<a href="'.U($info['class_id']).'">'.$info['class_name'].'</a>';
			}
		}
		
		return $pos;
	}
	
	//写入
	private function filePutContents($filepath,$tpl){
		ob_start();
		$default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
		$this->filterView();
		$content = $this->view->fetch('index@'.$default_themes.'/'.$tpl);
		
		echo $content;
		$_cache=ob_get_contents();
		ob_end_clean();
		
		if($_cache){
			$File = new \think\template\driver\File();
			$File->write($filepath, $_cache);	
		}
	}
	
	//获取列表页面的分页参数
	private function getListPageSize($info){
		$default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
		$tpl = $info['list_tpl'];
		$content = file_get_contents(app()->getRootPath().'app/index/view/'.$default_themes.'/'.$tpl.'.html');
		if($content){
			preg_match_all('/\{list(.*)num=[\'\"](\d+)[\'\"](.*)\}/',$content,$res);
			return $res[2][0];
		}
	}
	

}