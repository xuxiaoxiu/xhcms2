<?php

namespace xhadmin\db;
use \think\facade\Log; 


class Common
{
	
	//记录sql错误日志
	public static function setLog($msg){
		if(strpos($msg,'Duplicate') > 0){
			preg_match("/Duplicate entry '(.*)' for key '(.*)'/",$msg,$all);
			$fieldInfo = \app\admin\db\Field::getWhereInfo(['field'=>$all[2]]);
			if($fieldInfo){
				throw new \Exception($fieldInfo['name'].'已经存在!');
			}
		}
		log::error('sql错误：'.$msg);	
	}
	
	
    
}
